﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace otus_csharp_home_work_lesson_8
{
    class Program
    {
        private static readonly int countsIteration = 1_000;

        static void Main(string[] args)
        {
            var testData = TestData.Get();

            #region my serilization
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var str1 = MySerialization(testData);
            sw.Stop();
            Console.WriteLine($"MySerialization={sw.ElapsedMilliseconds}");
            #endregion

            #region my deserialization
            sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < countsIteration; i++)
            {
                TestData testData1 = MyDeserialization(str1);
            }
            sw.Stop();
            Console.WriteLine($"MyDeserialization={sw.ElapsedMilliseconds}");
            #endregion

            #region Newtonsoft.Json serialization
            sw = new Stopwatch();
            sw.Start();
            var str2 = NewtonsoftJsonSerialization(testData);
            sw.Stop();
            Console.WriteLine($"Newtonsoft.Json.Serialization={sw.ElapsedMilliseconds}");
            #endregion

            #region Newtonsoft.Json deserialization
            sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < countsIteration; i++)
            {
                TestData testData2 = NewtonsoftJsonDeserialization(str2);
            }
            sw.Stop();
            Console.WriteLine($"Newtonsoft.Json.Deserialization={sw.ElapsedMilliseconds}");
            #endregion

            Console.ReadKey();
        }

        private static string MySerialization(TestData testData)
        {
            string str = "";
            for (int i = 0; i < countsIteration; i++)
            {
                str = SerialozatorExt.ToCsv(testData);
            }
            return str;
        }

        private static TestData MyDeserialization(string str1)
        {
            return SerialozatorExt.FromCsv<TestData>(str1);
        }

        private static string NewtonsoftJsonSerialization(TestData testData)
        {
            string str = "";
            for (int i = 0; i < countsIteration; i++)
            {
                str = JsonConvert.SerializeObject(testData);
            }
            return str;
        }

        private static TestData NewtonsoftJsonDeserialization(string str)
        {
            return JsonConvert.DeserializeObject<TestData>(str);
        }
    }
}
