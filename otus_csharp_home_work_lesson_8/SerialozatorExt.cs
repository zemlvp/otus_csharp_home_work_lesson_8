﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;

namespace otus_csharp_home_work_lesson_8
{
    public static class SerialozatorExt
    {
        public static string ToCsv(this object obj)
        {
            if (obj == null)
                return null;
            
            var fields = obj.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            return string.Join('\t', fields.Select(a => a.Name + "=" + a.GetValue(obj))); ;
        }

        public static T FromCsv<T>(this string objStr) where T : new()
        {
            if (objStr == null)
                return new T(); 

            T result = new T();
            
            var str = objStr.Split('\t');

            var type = result.GetType();
            var fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

            foreach (var a in str)
            {
                var fieldStr = a.Split('=');
                var fieldName = fieldStr[0];

                var fieldType = fields.SingleOrDefault(a => a.Name == fieldName).FieldType;
                var fieldValue = Convert.ChangeType(fieldStr[1], fieldType);

                var fieldInfo = type.GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);
                fieldInfo.SetValue(result, fieldValue);
            }

            return result;
        }
    }
}
