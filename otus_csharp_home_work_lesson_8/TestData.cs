﻿
namespace otus_csharp_home_work_lesson_8
{
    public class TestData
    {
        int i1, i2, i3, i4;
        double i5;

        public static TestData Get() => new TestData()
        {
            i1 = 1,
            i2 = 2,
            i3 = 3,
            i4 = 4,
            i5 = 5.1
        };
    }
}
